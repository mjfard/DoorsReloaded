﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Doors.Domain._SeedWork;

namespace Doors.Domain.UserAggregate
{
    public class User : Entity
    {
        protected User(Guid id, string userName) : base(id)
        {
            UserName = userName;
        }

        public string UserName { get; }
    }
}
