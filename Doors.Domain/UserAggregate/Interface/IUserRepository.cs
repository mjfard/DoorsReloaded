﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Doors.Domain._SeedWork;

namespace Doors.Domain.UserAggregate.Interface
{
    public interface IUserRepository : IUnitOfWork
    {
        Task Add(User user);
    }
}
