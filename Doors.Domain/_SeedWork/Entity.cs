﻿using System;

namespace Doors.Domain._SeedWork
{
    public class Entity
    {
        protected Entity(Guid id)
        {
            Id = id;
        }


        public Guid Id { protected set; get; }
    }
}
