﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Doors.Domain._SeedWork
{
    public interface IUnitOfWork
    {
        Task<int> Save();
    }
}
