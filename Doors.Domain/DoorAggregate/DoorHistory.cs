﻿using System;
using Doors.Domain._SeedWork;

namespace Doors.Domain.DoorAggregate
{
    public class DoorHistory : Entity
    {
        public DoorHistory(Guid id, Guid doorId, DateTime dateTime, bool opened) : base(id)
        {
            DoorId = doorId;
            DateTime = dateTime;
            Opened = opened;
        }

        public Guid DoorId { get; }
        public DateTime DateTime { get; }
        public bool Opened { get; }
    }
}
