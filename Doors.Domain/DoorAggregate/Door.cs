﻿using System;
using System.Collections.Generic;
using System.Linq;
using Doors.Domain._SeedWork;
using Doors.Domain.DoorAggregate.Validations;

namespace Doors.Domain.DoorAggregate
{
    public class Door : Entity, IAggregateRoot
    {
        public Door(
            Guid id, 
            string doorName, 
            bool isOpen) : base(id)
        {
            Id = id;
            DoorName = doorName;
            IsOpen = isOpen;
        }
        public Guid Id { protected set; get; }

        public string DoorName { private set; get; }
        public bool IsOpen { private set; get; }
        public List<Permission> Permissions { private set; get; } = new List<Permission>();

        public void Open(Guid userId)
        {
            if (Permissions.All(p => p.UserId != userId))
                throw new UserDoesNotHavePermissionException(DoorName, userId);
            if (IsOpen)
                throw new DoorIsAlreadyOpenException(DoorName);
            IsOpen = true;
        }

        public void Close(Guid? userId)
        {
            if (userId == null)
                throw new UserIsNotKnownException();
            if (Permissions.All(p => p.UserId != userId))
                throw new UserDoesNotHavePermissionException(DoorName, userId.Value);
            if (IsOpen == false)
                throw new DoorIsAlreadyClosedException(DoorName);
            IsOpen = false;
        }

        public void AddPermission(Guid userId)
        {
            if (Permissions.Any(p => p.UserId == userId))
                return;
            Permissions.Add(new Permission(Guid.NewGuid(), userId));
        }
    }
}
