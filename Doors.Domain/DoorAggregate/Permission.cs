﻿using System;

namespace Doors.Domain.DoorAggregate
{
    public class Permission
    {
        public Permission(Guid permissionId, Guid userId)
        {
            UserId = userId;
            PermissionId = permissionId;
        }

        public Guid UserId { private set;get; }
        public Guid PermissionId { private set; get; }
        
        // other properties might be added later, like permitted time intervals
    }
}
