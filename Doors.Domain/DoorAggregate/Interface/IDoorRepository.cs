﻿using System;
using System.Threading.Tasks;
using Doors.Domain._SeedWork;

namespace Doors.Domain.DoorAggregate.Repos
{
    public interface IDoorRepository : IUnitOfWork
    {
        Task Add(Door door);
        Task<Door> Find(Guid doorId);
    }
}
