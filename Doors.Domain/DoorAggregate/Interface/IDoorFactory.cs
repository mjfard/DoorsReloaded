﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Doors.Domain.DoorAggregate.Interface
{
    public interface IDoorFactory
    {
        Door NewDoor(string doorName);
    }
}
