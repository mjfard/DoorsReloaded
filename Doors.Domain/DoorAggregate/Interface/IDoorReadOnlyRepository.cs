﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Doors.Domain.DoorAggregate.Repos
{
    public interface IDoorReadOnlyRepository
    {
        Task<List<Door>> GetAllDoors();
    }
}
