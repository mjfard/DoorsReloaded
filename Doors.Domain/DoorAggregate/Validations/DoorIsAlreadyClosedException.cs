﻿using System;

namespace Doors.Domain.DoorAggregate.Validations
{
    public class DoorIsAlreadyClosedException : Exception
    {
        public DoorIsAlreadyClosedException(string doorName) : base (doorName)
        {
            
        }
    }
}
