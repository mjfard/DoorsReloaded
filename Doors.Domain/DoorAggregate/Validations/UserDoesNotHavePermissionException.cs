﻿using System;

namespace Doors.Domain.DoorAggregate.Validations
{
    public class UserDoesNotHavePermissionException : Exception
    {
        public UserDoesNotHavePermissionException(string doorName, Guid userId) : base($"userId:{userId}, door: {doorName}")
        {
            
        }
    }
}
