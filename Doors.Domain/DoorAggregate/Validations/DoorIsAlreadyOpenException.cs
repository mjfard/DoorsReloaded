﻿using System;

namespace Doors.Domain.DoorAggregate.Validations
{
    public class DoorIsAlreadyOpenException : Exception
    {
        public DoorIsAlreadyOpenException(string doorName) : base (doorName)
        {
            
        }
    }
}
