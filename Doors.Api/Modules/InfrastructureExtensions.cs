﻿using System;
using Doors.Application.Services;
using Doors.Domain.DoorAggregate.Interface;
using Doors.Domain.DoorAggregate.Repos;
using Doors.Infrastructure;
using Doors.Infrastructure.DataAccess;
using Doors.Infrastructure.DataAccessReadOnly;
using Doors.Infrastructure.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Doors.Api.Modules
{
    public static class InfrastructureExtensions
    {
        public static void AddDependencyInjectionConfiguration(IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            // Application
            //            services.AddScoped<ICustomerAppService, CustomerAppService>();

            // Infrastructure
            services.AddScoped<IDoorFactory, DoorFactory>();
            
            services.AddScoped<IDoorRepository, DoorRepository>();
            services.AddScoped<DoorsDbContext>();

            services.AddScoped<IDoorReadOnlyRepository, DoorReadOnlyRepository>();
            services.AddScoped<DoorsReadOnlyDbContext>();
            
            services.AddScoped<IUserService, UserService>();
        }
    }
}