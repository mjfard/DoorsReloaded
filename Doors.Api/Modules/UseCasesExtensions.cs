﻿using Doors.Application.UseCases.DoorUsecases;
using Doors.Application.UseCases.DoorUsecases.Interface;
using Microsoft.Extensions.DependencyInjection;

namespace Doors.Api.Modules
{
    public static class UseCasesExtensions
    {
        public static IServiceCollection AddUseCases(this IServiceCollection services)
        {
            services.AddScoped<IAddDoorUseCase, AddDoorUseCase>();
            services.AddScoped<ICloseDoorUserCase, CloseDoorUseCase>();
            services.AddScoped<IGetAllDoorsUseCase, GetAllDoorsUseCase>();
            return services;
        }

    }
}
