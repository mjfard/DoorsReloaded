﻿using System;
using Doors.Application.UseCases.DoorUsecases.Interface;
using Microsoft.AspNetCore.Mvc;

namespace Doors.Api.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("api/v1/Door/Close")]
    [ApiController]
    public class CloseDoorController : ApiController
    {
        private readonly ICloseDoorUserCase _usecase;

        public CloseDoorController(ICloseDoorUserCase usecase)
        {
            _usecase = usecase;
        }
        
        [HttpPut]
        public void CloseDoor(Guid doorId)
        {
            _usecase.Execute(doorId);
        }
        
    }
}
