﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Doors.Application.UseCases.DoorUsecases.Interface;
using Doors.Application.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Doors.Api.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("api/v1/Door/GetAll")]
    [ApiController]
    public class GetAllDoorsController : ApiController
    {
        private readonly IGetAllDoorsUseCase _usecase;

        public GetAllDoorsController(IGetAllDoorsUseCase usecase)
        {
            _usecase = usecase;
        }
        
        [HttpGet]
        public async Task<List<DoorVM>> GetAllDoors()
        {
            return await _usecase.Execute();
        }
        
    }
}
