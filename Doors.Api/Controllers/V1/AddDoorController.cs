﻿using System.Threading.Tasks;
using Doors.Application.UseCases.DoorUsecases.Interface;
using Microsoft.AspNetCore.Mvc;

namespace Doors.Api.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("api/v1/Door")]
    [ApiController]
    public class AddDoorController : ApiController
    {
        private readonly IAddDoorUseCase _useCase;

        public AddDoorController(IAddDoorUseCase useCase)
        {
            _useCase = useCase;
        }

        [HttpPost]
        public async Task AddDoor(string doorName)
        {
            await _useCase.Execute(doorName);
        }
        
    }
}
