﻿using System;
using Doors.Infrastructure;
using Doors.Infrastructure.DataAccess;
using Doors.Infrastructure.DataAccessReadOnly;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NetDevPack.Identity;

namespace Doors.Api.Configurations
{
    public static class DatabaseConfig
    {
        public static void AddDatabaseConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddDbContext<DoorsDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
            
            services.AddDbContext<DoorsReadOnlyDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("ReadOnlyConnection")));
            
            services.AddIdentityEntityFrameworkContextConfiguration(options =>
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"),
                    b => b.MigrationsAssembly("Doors.Infrastructure")));

        }
    }
}