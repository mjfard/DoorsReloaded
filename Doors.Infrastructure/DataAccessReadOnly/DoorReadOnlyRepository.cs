﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Doors.Domain.DoorAggregate;
using Doors.Domain.DoorAggregate.Repos;
using Microsoft.EntityFrameworkCore;

namespace Doors.Infrastructure.DataAccessReadOnly
{
    public class DoorReadOnlyRepository : IDoorReadOnlyRepository
    {
        private readonly DoorsReadOnlyDbContext _dbContext;

        public DoorReadOnlyRepository(DoorsReadOnlyDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<List<Door>> GetAllDoors()
        {
            return await _dbContext.Doors.ToListAsync();
        }
    }
}
