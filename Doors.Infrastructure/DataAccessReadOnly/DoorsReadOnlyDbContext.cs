﻿using Doors.Infrastructure.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace Doors.Infrastructure.DataAccessReadOnly
{
    public class DoorsReadOnlyDbContext : DoorsDbContext
    {
        public DoorsReadOnlyDbContext(DbContextOptions<DoorsDbContext> coreOptions) : base(coreOptions)
        {
        }
    }
}
