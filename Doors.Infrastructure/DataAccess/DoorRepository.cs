﻿using System;
using System.Threading.Tasks;
using Doors.Domain.DoorAggregate;
using Doors.Domain.DoorAggregate.Repos;
using Microsoft.EntityFrameworkCore;

namespace Doors.Infrastructure.DataAccess
{
    public class DoorRepository : IDoorRepository
    {
        private readonly DoorsDbContext _dbContext;

        public DoorRepository(DoorsDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<int> Save()
        {
            return await _dbContext.SaveChangesAsync();
        }

        public async Task Add(Door door)
        {
            await _dbContext.AddAsync(door);
        }

        public async Task<Door> Find(Guid doorId)
        {
            return await _dbContext.Doors.Include(d => d.Permissions).FirstOrDefaultAsync(d => d.Id == doorId);
        }
    }
}
