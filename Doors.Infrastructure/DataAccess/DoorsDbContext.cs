﻿using Doors.Domain.DoorAggregate;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Doors.Infrastructure.DataAccess
{
    public class DoorsDbContext : IdentityDbContext
    {
        public DoorsDbContext(DbContextOptions<DoorsDbContext> coreOptions) : base(coreOptions)
        {
        }

        public DbSet<Door> Doors { set; get; }
        
        public DbSet<Permission> Permissions { set; get; }
        
    }
}
