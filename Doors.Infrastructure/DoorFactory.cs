﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Doors.Domain.DoorAggregate;
using Doors.Domain.DoorAggregate.Interface;

namespace Doors.Infrastructure
{
    public class DoorFactory : IDoorFactory
    {
        public Door NewDoor(string doorName) => new Door(Guid.NewGuid(), doorName, false);
    }
}
