﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Doors.Application.ViewModels
{
    public class DoorVM
    {
        public string DoorName { set; get; }
        public bool IsOpen { set; get; }
        public bool CurUserHasPerm { set; get; }
    }
}
