﻿using System.Threading.Tasks;
using Doors.Domain.UserAggregate.Interface;

namespace Doors.Application.UseCases.UserUsecases
{
    public class AddUser
    {
        private readonly IUserRepository _repo;
        private readonly IUserFactory _factory;

        public AddUser(IUserRepository repo, IUserFactory factory)
        {
            _repo = repo;
            _factory = factory;
        }

        public async Task Execute(string userName)
        {
            var user = _factory.NewUser(userName);
            await _repo.Add(user);
            await _repo.Save();
        }
    }
}
