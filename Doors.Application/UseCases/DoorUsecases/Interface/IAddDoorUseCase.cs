﻿using System.Threading.Tasks;

namespace Doors.Application.UseCases.DoorUsecases.Interface
{
    public interface IAddDoorUseCase
    {
        Task Execute(string doorName);
    }
}