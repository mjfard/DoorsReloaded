﻿using System;
using System.Threading.Tasks;

namespace Doors.Application.UseCases.DoorUsecases.Interface
{
    public interface ICloseDoorUserCase
    {
        Task Execute(Guid doorId);
    }
}