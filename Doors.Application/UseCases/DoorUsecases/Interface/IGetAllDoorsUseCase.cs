﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Doors.Application.ViewModels;
using Doors.Domain.DoorAggregate;

namespace Doors.Application.UseCases.DoorUsecases.Interface
{
    public interface IGetAllDoorsUseCase
    {
        Task<List<DoorVM>> Execute();
    }
}
