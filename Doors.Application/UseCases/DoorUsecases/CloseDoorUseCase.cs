﻿using System;
using System.Threading.Tasks;
using Doors.Application.Services;
using Doors.Application.UseCases.DoorUsecases.Interface;
using Doors.Domain.DoorAggregate;
using Doors.Domain.DoorAggregate.Repos;

namespace Doors.Application.UseCases.DoorUsecases
{
    public class CloseDoorUseCase : ICloseDoorUserCase
    {
        private readonly IDoorRepository _repo;
        private readonly IUserService _userService;

        public CloseDoorUseCase(IDoorRepository repo, IUserService userService)
        {
            _repo = repo;
            _userService = userService;
        }
        public async Task Execute(Guid doorId)
        {
            var door = await _repo.Find(doorId);
            var currentUserId = _userService.GetCurrentUserId();
            door.Close(currentUserId.Value);
        }
    }
}
