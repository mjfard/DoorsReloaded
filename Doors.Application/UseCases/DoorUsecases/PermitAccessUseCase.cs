﻿using System;
using System.Threading.Tasks;
using Doors.Application.UseCases.DoorUsecases.Interface;
using Doors.Domain.DoorAggregate.Interface;
using Doors.Domain.DoorAggregate.Repos;

namespace Doors.Application.UseCases.DoorUsecases
{
    class PermitAccessUseCase : IPermitAccessUseCase
    {
        private readonly IDoorRepository _repo;
        private readonly IDoorFactory _factory;

        public PermitAccessUseCase(IDoorRepository repo, IDoorFactory factory)
        {
            _repo = repo;
            _factory = factory;
        }
        public async Task Execute(Guid userId, Guid doorId)
        {
            var door = await _repo.Find(doorId);
            door.AddPermission(userId);
            await _repo.Save();
        }
    }
}
