﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Doors.Application.Services;
using Doors.Application.UseCases.DoorUsecases.Interface;
using Doors.Application.ViewModels;
using Doors.Domain.DoorAggregate;
using Doors.Domain.DoorAggregate.Repos;

namespace Doors.Application.UseCases.DoorUsecases
{
    public class GetAllDoorsUseCase : IGetAllDoorsUseCase
    {
        private readonly IDoorReadOnlyRepository _readOnlyRepository;
        private readonly IUserService _userService;

        public GetAllDoorsUseCase(IDoorReadOnlyRepository readOnlyRepository, IUserService userService)
        {
            _readOnlyRepository = readOnlyRepository;
            _userService = userService;
        }

        public async Task<List<DoorVM>> Execute()
        {
            var doors =  await _readOnlyRepository.GetAllDoors();
            var curUserId = _userService.GetCurrentUserId();
            return doors.Select(d => new DoorVM()
            {
                DoorName = d.DoorName,
                IsOpen = d.IsOpen,
                CurUserHasPerm = d.Permissions.Any(p => p.UserId == curUserId)
            }).ToList();
        }
    }
}
