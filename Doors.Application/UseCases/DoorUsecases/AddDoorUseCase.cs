﻿using System.Threading.Tasks;
using Doors.Application.UseCases.DoorUsecases.Interface;
using Doors.Domain.DoorAggregate.Interface;
using Doors.Domain.DoorAggregate.Repos;

namespace Doors.Application.UseCases.DoorUsecases
{
    public class AddDoorUseCase : IAddDoorUseCase
    {
        private readonly IDoorRepository _repo;
        private readonly IDoorFactory _factory;

        public AddDoorUseCase(IDoorRepository repo, IDoorFactory factory)
        {
            _repo = repo;
            _factory = factory;
        }

        public async Task Execute(string doorName)
        {
            var door = _factory.NewDoor(doorName);
            await _repo.Add(door);
            await _repo.Save();
        }
    }
}
